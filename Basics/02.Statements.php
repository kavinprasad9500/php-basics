<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Statments</title>
</head>
<body>


    <?php

    // ifElse Statement
    // $payment = false;
    // if($payment){
    //     echo 'Payment Success';
    // } else {
    //     echo 'Buy Now';
    // }


    // Elseif Statement 
    // $user = 'teacher';
    // if ($user == 'admin'){
    //     echo 'Hi Admin';
    // }elseif ($user == 'student'){
    //     echo 'Hi Student';
    // }elseif ($user == 'teacher'){
    //     echo 'Hi Teacher';
    // }else{
    //     echo 'Hi Anonymous';
    // }


    // Switch Statement
    $userRating = 1;
    switch ($userRating){
        case 1:
            echo '1 Star';
            break;
        case 2:
            echo '2 Star';
            break;
        case 3:
            echo '3 Star';
            break;
    }

    ?>


    <!-- Comparision Operators
    ==
    ===
    >
    <
    >=
    <=
    !=
    ###############
    Logical Operators 
    &&
    ||
    ############### -->

</body>
</html>