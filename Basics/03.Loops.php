<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Loops</title>
</head>
<body>
    <?php


    // While Loop 
    // $number = 0;
    // while($number <= 10){
    //     echo $number;
    //     $number = $number + 1;//$number = $number++;
    // }


    // For Loop
    // for($number; $number < 10; $number++) {
    //     echo $number . '<br>';
    // }

    // ForEach Loop
    // Using Only On Arrays
    $numbers = [1,2,3,4,5,6,7,8,9,10];
    
    foreach($numbers as $number) {
        echo $number . '<br>';
    }


    


    ?>
</body>
</html>