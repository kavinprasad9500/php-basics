-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2022 at 12:35 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `result`
--

-- --------------------------------------------------------

--
-- Table structure for table `cse`
--

CREATE TABLE `cse` (
  `register_no` int(12) NOT NULL,
  `name` varchar(32) NOT NULL,
  `department` varchar(64) NOT NULL,
  `dob` varchar(12) NOT NULL,
  `paper_01` varchar(128) NOT NULL,
  `code_01` varchar(12) NOT NULL,
  `grade_01` varchar(4) NOT NULL,
  `result_01` varchar(4) NOT NULL,
  `paper_02` varchar(128) NOT NULL,
  `code_02` varchar(12) NOT NULL,
  `grade_02` varchar(4) NOT NULL,
  `result_02` varchar(4) NOT NULL,
  `paper_03` varchar(128) NOT NULL,
  `code_03` varchar(12) NOT NULL,
  `grade_03` varchar(4) NOT NULL,
  `result_03` varchar(4) NOT NULL,
  `paper_04` varchar(128) NOT NULL,
  `code_04` varchar(12) NOT NULL,
  `grade_04` varchar(4) NOT NULL,
  `result_04` varchar(4) NOT NULL,
  `paper_05` varchar(128) NOT NULL,
  `code_05` varchar(12) NOT NULL,
  `grade_05` varchar(4) NOT NULL,
  `result_05` varchar(4) NOT NULL,
  `paper_06` varchar(128) NOT NULL,
  `code_06` varchar(12) NOT NULL,
  `grade_06` varchar(4) NOT NULL,
  `result_06` varchar(4) NOT NULL,
  `paper_07` varchar(128) NOT NULL,
  `code_07` varchar(12) NOT NULL,
  `grade_07` varchar(4) NOT NULL,
  `result_07` varchar(4) NOT NULL,
  `paper_08` varchar(128) NOT NULL,
  `code_08` varchar(12) NOT NULL,
  `grade_08` varchar(4) NOT NULL,
  `result_08` varchar(4) NOT NULL,
  `paper_09` varchar(128) NOT NULL,
  `code_09` varchar(12) NOT NULL,
  `grade_09` varchar(4) NOT NULL,
  `result_09` varchar(4) NOT NULL,
  `paper_10` varchar(128) NOT NULL,
  `code_10` varchar(12) NOT NULL,
  `grade_10` varchar(4) NOT NULL,
  `result_10` varchar(4) NOT NULL,
  `total` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cse`
--

INSERT INTO `cse` (`register_no`, `name`, `department`, `dob`, `paper_01`, `code_01`, `grade_01`, `result_01`, `paper_02`, `code_02`, `grade_02`, `result_02`, `paper_03`, `code_03`, `grade_03`, `result_03`, `paper_04`, `code_04`, `grade_04`, `result_04`, `paper_05`, `code_05`, `grade_05`, `result_05`, `paper_06`, `code_06`, `grade_06`, `result_06`, `paper_07`, `code_07`, `grade_07`, `result_07`, `paper_08`, `code_08`, `grade_08`, `result_08`, `paper_09`, `code_09`, `grade_09`, `result_09`, `paper_10`, `code_10`, `grade_10`, `result_10`, `total`) VALUES
(20104006, 'AKilan B', 'Computer Science and Engineering', '2003-07-27', 'Probability and Stochastic Models', '19BS405', '-', 'Fail', 'Design and Analysis of Algorithms', '19CS401', '-', 'Fail', 'Universal Human Values', '19HS402', '-', 'Fail', 'Computer Networks', '19CS403', '-', 'Fail', 'Operating Systems', '19CS404', '-', 'Fail', 'Database Management Systems', '19IT302', '-', 'Fail', 'Quantitative Aptitude and Logical Reasoning -II', '19TPS04', '-', 'Fail', 'Database Laboratory', '19CS405', '-', 'Fail', 'Networking Laboratory', '19CS406', '-', 'Fail', 'Language Skills\r\n', '19HS401', '-', 'Fail', 'Fail'),
(20104011, 'Arul Bruno X', 'Computer Science and Engineering', '2003-09-16', 'Probability and Stochastic Models', '19BS405', 'A', 'Pass', 'Design and Analysis of Algorithms', '19CS401', 'O', 'Pass', 'Universal Human Values', '19HS402', 'O', 'Pass', 'Computer Networks', '19CS403', 'O', 'Pass', 'Operating Systems', '19CS404', 'O', 'Pass', 'Database Management Systems', '19IT302', 'O', 'Pass', 'Quantitative Aptitude and Logical Reasoning -II', '19TPS04', 'O', 'Pass', 'Database Laboratory', '19CS405', '-', 'Fail', 'Networking Laboratory', '19CS406', '-', 'Fail', 'Language Skills\r\n', '19HS401', '-', 'Fail', 'Fail'),
(20104034, 'Esakkimuthu K', 'Computer Science and Engineering', '2003-05-26', 'Probability and Stochastic Models', '19BS405', 'O', 'Pass', 'Design and Analysis of Algorithms', '19CS401', 'O', 'Pass', 'Universal Human Values', '19HS402', 'O', 'Pass', 'Computer Networks', '19CS403', 'O', 'Pass', 'Operating Systems', '19CS404', 'O', 'Pass', 'Database Management Systems', '19IT302', 'O', 'Pass', 'Quantitative Aptitude and Logical Reasoning -II', '19TPS04', 'O', 'Pass', 'Database Laboratory', '19CS405', 'O', 'Pass', 'Networking Laboratory', '19CS406', 'O', 'Pass', 'Language Skills', '19HS401', 'O', 'Pass', 'Pass'),
(20104055, 'Kavin Prasad J', 'Computer Science and Engineering', '2003-03-27', 'Probability and Stochastic Models', '19BS405', 'O', 'Pass', 'Design and Analysis of Algorithms', '19CS401', 'O', 'Pass', 'Universal Human Values', '19HS402', 'O', 'Pass', 'Computer Networks', '19CS403', 'O', 'Pass', 'Operating Systems', '19CS404', 'O', 'Pass', 'Database Management Systems', '19IT302', 'O', 'Pass', 'Quantitative Aptitude and Logical Reasoning -II', '19TPS04', 'O', 'Pass', 'Database Laboratory', '19CS405', 'O', 'Pass', 'Networking Laboratory', '19CS406', 'O', 'Pass', 'Language Skills\r\n', '19HS401', 'O', 'Pass', 'Pass'),
(20104091, ' Praveen Kumar B', 'Computer Science and Engineering', '2002-02-28', 'Probability and Stochastic Models', '19BS405', 'A', 'Pass', 'Design and Analysis of Algorithms', '19CS401', 'O', 'Pass', 'Universal Human Values', '19HS402', 'A', 'Pass', 'Computer Networks', '19CS403', 'O', 'Pass', 'Operating Systems', '19CS404', 'O', 'Pass', 'Database Management Systems', '19IT302', 'O', 'Pass', 'Quantitative Aptitude and Logical Reasoning -II', '19TPS04', 'A', 'Pass', 'Database Laboratory', '19CS405', 'O', 'Pass', 'Networking Laboratory', '19CS406', 'A', 'Pass', 'Language Skills', '19HS401', 'O', 'Pass', 'Pass');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cse`
--
ALTER TABLE `cse`
  ADD PRIMARY KEY (`register_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cse`
--
ALTER TABLE `cse`
  MODIFY `register_no` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20104092;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
