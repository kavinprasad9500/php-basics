<?php

    if(isset($_SESSION['email'])){
        $current_user = $_SESSION['email'];
   
        $select_all_query = "SELECT * FROM users WHERE user_email = '$current_user' ";
        $select_all_result = mysqli_query($connection,$select_all_query);
        while($row = mysqli_fetch_assoc($select_all_result)){
            $username = $row['username'];
            $user_firstname = $row['user_firstname'];
            $user_lastname = $row['user_lastname'];
            $user_email = $row['user_email'];
            $user_password = $row['user_password'];
            $user_image = $row['user_image'];
        }

    }

    if(isset($_POST['update_user'])){
        
        $username = $_POST['username'];
        $user_firstname =$_POST['user_firstname'];
        $user_lastname = $_POST['user_lastname'];
        $user_email =$_POST['user_email'];
        $user_password = $_POST['user_password'];

        // $user_image = $_FILES['image']['name'];
        // $user_image_temp = $_FILES['image']['tmp_name'];
        
        // move_uploaded_file($user_image_temp,"../img/$user_image");        

        if(empty($user_image)){
            $user_query = "SELECT * FROM users WHERE user_email = '$current_user' ";
            $user_result = mysqli_query($connection,$user_query);

            while ($row = mysqli_fetch_assoc($user_result)){
                $user_image = $row['user_image'];
            }
        }



        $update_user_query = "UPDATE users SET username = '$username',user_firstname='$user_firstname',user_lastname='$user_lastname',user_email='$user_email',user_password='$user_password',user_image='$user_image' WHERE user_email ='$user_email' ";
      
        $update_user_result = mysqli_query($connection,$update_user_query);
        if(!$update_user_result){
            die('Query Failed').mysqli_error($connection);
        }
    
    }

?>


<!-- Bar Chart  -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m0 font-weight-bold text-primary">Edit User</h6>
    </div>
    <div class="card-body">
        <form action="profile.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title">User Name</label>
                <input type="text" value="<?php echo $username;?>" class="form-control" name="username">
            </div>
            <div class="form-group">
                <label for="title">First Name</label>
                <input type="text" value="<?php echo $user_firstname; ?>" class="form-control" name="user_firstname">
            </div>
            <div class="form-group">
                <label for="title">Last Name</label>
                <input type="text" value="<?php echo $user_lastname; ?>" class="form-control" name="user_lastname">
            </div>
            <div class="form-group">
                <label for="post_image">User Image</label><br>
                <img width="200px" src="../images/<?php echo $user_image; ?>" alt="" srcset=""><br><br>
                <input type="file" value="" name="user_image">
            </div>
            <div class="form-group">
                <label for="title">User Email</label>
                <input type="text" value="<?php echo $user_email; ?>" class="form-control" name="user_email">
            </div>
            <div class="form-group">
                <label for="title">User Password</label>
                <input type="password" value="<?php echo $user_password; ?>" class="form-control" name="user_password">
            </div>
            <div class="form-group">
                <input type="submit" value="Update User" class="btn btn-primary" name="update_user">
            </div>
        </form>
    </div>
</div>


<?php


    // $post_title = $_POST['post_title'];
    // $post_category_id =$_POST['post_category_id'];
    // $post_author = $_POST['post_author'];
    // $post_status =$_POST['post_status'];

    // $post_image = $_FILES['image']['name'];
    // $post_image_temp = $_FILES['image']['tmp_name'];

    // $post_tags = $_POST['post_tags'];
    // $post_content = $_POST['post_content'];
    // $post_date = date('d-m-y');
    // $post_comment_count = 4;


?>