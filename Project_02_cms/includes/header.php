<?php

session_start();

if(!isset($_SESSION['role'])){
    $_SESSION['role']= 'user';
    $_SESSION['username'] = 'user';
}


?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Unic</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: 'Quicksand', sans-serif;
            } 
        </style>
    </head>
    <body> 
        <!-- Responsive navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="#!">Unic Fort</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item"><a class="nav-link active" aria-current="page" href="index.php">Home</a></li>
                        
                        <?php
                        $select_all_category = "SELECT * FROM category LIMIT 4";
                        $menu_result = mysqli_query($connection,$select_all_category);

                        while($row = mysqli_fetch_assoc($menu_result)) {
                            $cat_title = $row ['category_title'];
                            echo "<li class='nav-item'><a class='nav-link' href='*'>{$cat_title}</a></li>";
                        }
                        ?>
                        <?php
                            if(isset($_SESSION['role'])){
                                if($_SESSION['role']=='admin') {
                                    echo "<li class='nav-item'>
                                    <a class='nav-link' href='admin/'>Admin</a>
                                </li>";
                                }
                            }
                        ?>
                        
                        <li class='nav-item'>
                            <a class='nav-link' href='admin/login.php'>Login</a>
                        </li>
                        <?php
                            if(isset($_SESSION['role'])){
                                if($_SESSION['role']=='admin') {
                                if(isset($_GET['post'])){
                                    $post_id =$_GET['post'];
                                    echo "<li class='nav-item'>
                                      <a class='nav-link' href='admin/post.php?page=edit_post&post_id=$post_id'>Edit</a>
                                      </li>";
                                }
                                }
                            }
                        ?>
        
                    </ul>
                </div>
            </div>
        </nav>