<?php


include 'includes/db.php';
include 'includes/header.php';

$search = $_POST['search'];


?>
        <!-- Page content-->
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-8">
                    <!-- Post content-->

                    <?php
                    $search_query = "SELECT * FROM posts WHERE post_title LIKE '%$search%'";
                    $search_result = mysqli_query($connection,$search_query);


                    $post_count = mysqli_num_rows($search_result);

                    if($post_count === 0) {
                        echo '<h1> No Results Found </h1>';
                    }

                    while ($row = mysqli_fetch_assoc($search_result)){
                        $post_title = $row['post_title'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_content = $row['post_content'];
                        $post_image = $row['post_image'];
                        $post_tags = $row['post_tags'];


                    ?>


                    <article>
                        <!-- Preview image figure-->
                        <figure class="mb-4"><img class="img-fluid rounded" src="images/<?Php echo $post_image ?>" alt="moneyheist_image" /></figure>
                        <!-- Post content-->
                        <section class="mb-5">
                            <h2 class="fw-bolder mb-4 mt-5"><?php echo $post_title;?></h2>
                            <p class="fs-5 mb-4"><?php echo $post_content;?></p>
                        </section>
                    </article>
                    
                    <?php
                    }
                    ?>
                </div>
                
                

                

                <!-- Side widgets-->
                <?php include 'includes/sidebar.php'?>


    <?php include 'includes/footer.php'?>
