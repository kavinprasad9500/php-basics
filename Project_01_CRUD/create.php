<?php

include 'db.php';
include 'function.php';

if (isset($_POST['create'])){
	create();
}

// HEADER PHP CONNECTION
	include 'includes/header.php';
?>


	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
				<form class="login100-form validate-form" action="create.php" method="post">
					<span class="login100-form-title p-b-33">
						CREATE
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="wrap-input100 rs1 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="container-login100-form-btn m-t-20">
						<button class="login100-form-btn" name="create">
							Create
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	

<!-- FOOTER PHP CONNECTIONS  -->
<?php
include 'includes/footer.php';
?>