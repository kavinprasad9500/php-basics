<?php


include 'db.php'; 


    function showAllData(){
        global $connection;
        $query = "SELECT * FROM users";
        $result = mysqli_query($connection, $query);
        if(!$result) {
            die('Query Failed') . mysqli_error($connection);
        }
        while ($row = mysqli_fetch_assoc($result)) {
            $id = $row['id'];
            echo "<option value = '$id'>$id</option>";
        }
    }


    function create(){
        global $connection;
        $username = mysqli_real_escape_string($connection,$_POST['username']);
        $password = mysqli_real_escape_string($connection,$_POST['password']);
        $password = crypt($password,'$2a$07$usesomesillystringkavinpra$');
        // echo $username." ".$password;
        // $connection = mysqli_connect('localhost','root','','first_data_base');
        // if(!$connection){
        // 	die('Not Connected') ;
        // }
        $query = "INSERT INTO users(username,password) VALUES ('$username','$password')";
        $result = mysqli_query($connection, $query);
        if(!$result) {
            die('Query Failed') . mysqli_error($connection);
        }
    }


    function read(){
        global $connection;
        $query = "SELECT * FROM users";
        $result = mysqli_query($connection,$query);
        if(!$result) {
            die('Query Failed') . mysqli_error($connection);
        }
        return $result;
    }


    function update(){
        global $connection;
        $username = $_POST['username'];
	    $password = $_POST['password'];
	    $id = $_POST['id'];
	    $query = "UPDATE users SET username = '$username', password ='$password' WHERE id = $id";
	    $result = mysqli_query($connection,$query);
    }


    function delete(){
        global $connection;
        $username = $_POST['username'];
        // $password = $_POST['password'];
        // $id = $_POST['id'];
        // $query = "DELETE FROM users WHERE id = $id";
        $query = "DELETE FROM users WHERE username = '$username'";
        $result = mysqli_query($connection,$query);
    }


?>