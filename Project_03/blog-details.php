<?php
include 'includes/db.php';
include 'includes/header.php';
?>

<div class="container mt-5">
    <div class="row">
        <article>

        <?php

            if(isset($_GET['post'])){
                $post_id =$_GET['post'];
            }

            $post_query = "SELECT * FROM posts WHERE post_id = $post_id";
            $post_result = mysqli_query($connection,$post_query);
            while ($row = mysqli_fetch_assoc($post_result)){
                $post_title = $row['post_title'];
                $post_author = $row['post_author'];
                $post_date = $row['post_date'];
                $post_content = $row['post_content'];
                $post_image = $row['post_image'];
                $post_tags = $row['post_tags'];
            }
        ?>
            <!-- Preview image figure-->

            <figure class="mb-4"><img class="img-fluid rounded" src="images/<?php echo $post_image ?>" alt=".."/></figure>
            <!-- Post content-->
            <section class="mb-5">
                <h2 class="fw-bolder mb-4 mt-5"><?php echo $post_title ?></h2>
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Post Author : <p class="card-text"><?php echo $post_author?></p></h5>
                        <p class="card-text">Date : <?php echo $post_date?></p>
                </div>
            </div>
            <h4 class="fs-5 mb-4"><?php echo $post_content;?></h4><br>
            </section>
        </article><br>
        <section>
            <div class="container">
            
                <div class="row">
                    <div class="col-sm-5 col-md-6 col-12 pb-4">
                        <h1>Comments</h1>
                        <?php
                        $select_all_query = "SELECT * FROM comments WHERE comment_post_id = $post_id AND comment_status = 'approved'";
                        $select_all_result = mysqli_query($connection,$select_all_query);
                        while($row = mysqli_fetch_assoc($select_all_result)){                                       
                            $comment_author = $row['comment_author'];
                            $comment_content = $row['comment_content'];
                            $comment_date = $row['comment_date'];
                        ?>
                        <div class="comment mt-4 text-justify float-left">
                            <img src="https://i.imgur.com/yTFUilP.jpg" alt="" class="rounded-circle" width="40" height="40">
                            <h4><?php echo $comment_author; ?></h4> 
                            <span>-<?php echo $comment_date; ?></span> <br>
                            <p><?php echo $comment_content; ?></p><br>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </section>




        <div class="comment-form container">
            <h4>Leave a Comment</h4>
            <form action="blog-details.php?post=<?php echo $post_id; ?>" method="post">
            <div class="form-group from-inline">
            </div>
            <div class="form-group">
                <input type="text" class="form-control mb-10" name="author" placeholder="Name"><br>
                <input type="email" class="form-control mb-10" name="email" placeholder="Email"><br>
                <textarea class="form-control mb-10" rows="5" name="message" placeholder="Message" onfocus="this.placeholder =''"onblur="this.placeholder='Message'" required="" id="" cols="30" ></textarea><br>
            </div>
            <input type="submit" value="Send Message" name="create_comment" class="btn btn-primary submit_btn">
        </form><br>
        </div>
    </div>
</div>

<?php    
if(isset($_POST['create_comment'])){
    $comment_author = $_POST['author'];
    $comment_email = $_POST['email'];
    $comment_message = $_POST['message'];

    
    $comment_query = "INSERT INTO comments (comment_post_id,comment_author,comment_email,comment_content,comment_status,comment_date) VALUES($post_id,'$comment_author' ,'$comment_email','$comment_message','unapproved',now())";


    $comment_result = mysqli_query($connection,$comment_query);
    if(!$comment_result) {
		die('Query Failed').mysqli_error($connection);
	}

}
?>



<?php
include 'includes/footer.php';
?>
