<?php
include 'includes/db.php';
include 'includes/header.php';

?>
        <!-- Page content-->
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-8">
                    <!-- Post content-->
                    <?php
                        if(isset($_GET['id'])){
                            $category_id = $_GET['id'];
                        }


                    $post_query = "SELECT * FROM posts WHERE post_category_id = $category_id";
                    $post_result = mysqli_query($connection,$post_query);

                    while ($row = mysqli_fetch_assoc($post_result)){
                        $post_title = $row['post_title'];
                        $post_author = $row['post_author'];
                        $post_date = $row['post_date'];
                        $post_content = substr($row['post_content'],0,100) ;
                        $post_image = $row['post_image'];
                        $post_tags = $row['post_tags'];
                        $post_id =$row['post_id'];
                    ?>


                    <article>
                        <!-- Preview image figure-->
                        <figure class="mb-4"><img class="img-fluid rounded" src="images/<?Php echo $post_image ?>" alt="moneyheist_image" /></figure>
                        <!-- Post content-->
                        <section class="mb-5">
                            <h2 class="fw-bolder mb-4 mt-5"><?php echo $post_title;?></h2>
                            <p class="fs-5 mb-4"><?php echo $post_content;?></p>
                            <a href="blog-details.php?post=<?php echo $post_id; ?>" class="btn btn-primary mt-15"style="border-radius: 25px;">Continue Reading</a>
                            <br>
                        </section><hr>
                    </article>
                    <?php
                    }
                    ?>

                </div>
                
                

                

                <!-- Side widgets-->
                <?php include 'includes/sidebar.php'?>


    <?php include 'includes/footer.php'?>
