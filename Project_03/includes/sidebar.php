<div class="col-lg-4">
                    <!-- Search widget-->
                    <div class="card mb-4">
                        <div class="card-body">
                            <form class="input-group" action="search.php" method="post">
                                <input type="search" name="search" class="form-control rounded" placeholder="search" aria-label="Search"aria-describedby="search-addon" />
                                <button type="submit" name="submit" class="btn btn-outline-primary">search</button>
                            </form>
                        </div>
                    </div>
                        
                    <!-- Categories widget-->
                    <div class="card mb-4">
                        <div class="card-header">Post Categories</div>
                        <div class="card-body">
                            <div class="row">               
                                <ul class="list-unstyled mb-0">
                                <?php
                                $select_all_category = "SELECT * FROM category";
                                $side_cat_result = mysqli_query($connection,$select_all_category);

                                while($row = mysqli_fetch_assoc($side_cat_result)) {
                                    $cat_title = $row ['category_title'];
                                    $cat_id = $row ['category_id'];
                                    echo "<li><a href='category.php?id={$cat_id}' class='justify-content-between '>{$cat_title}</a></li>";
                                }
                                ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
