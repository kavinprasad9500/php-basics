<?php 
    ob_start();
    include 'includes/header.php';
    include 'includes/sidebar.php';
    include 'includes/nav.php';
    include '../includes/db.php';


    if(isset($_POST['submit'])){
        $cat_title = $_POST['cat_title'];
        if(empty($cat_title)){
            echo 'This Field Cannot Be Empty';
        }else {
            $create_cat_query = "INSERT INTO category(category_title) VALUES ('$cat_title')";
            $create_cat_result = mysqli_query($connection,$create_cat_query);
            if(!$create_cat_result){
                echo 'Category Not Create';
            }
        }

    }

    ?>

<!-- Begin Page Content  -->
<div class="container-fluid">
    <!-- Content Row  -->
    <div class="row">
        <!-- Area Chart  -->
        <div class="col-xl-6 col-lg-6" style="height: 200px;">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-item-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Add Categories</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <form action="category.php" method="post">
                        <!-- <label for="cat-title">Add Categories: </label> -->
                        <input type="text" class="form-control" name="cat_title"><br>
                        <input type="submit" class="btn btn-primary" value="submit" name="submit">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-log-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">All Categories</h6>
                    <div class="dropdown no-arrow">
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Categories</th>
                                <th>Remove</th>
                                <th>Edit</th>
                                <!-- <th>Edit</th> --> 
                        </thead>
                        <tbody>
                            <?php
                            $select_all_query = "SELECT * FROM category";
                            $select_all_result = mysqli_query($connection,$select_all_query);
                            while($row = mysqli_fetch_assoc($select_all_result)){
                                $cat_title = $row['category_title'];
                                $cat_id = $row['category_id'];

                                echo "<tr>
                                        <td>{$cat_title}</td>
                                        <td><a href='category.php?delete={$cat_id}'>Delete</a></td>
                                        <td><a href='category.php?edit={$cat_id}'>Edit</a></td>
                                      </tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php


if(isset($_GET['edit'])){
    $edit_cat_id = $_GET['edit'];
    $select_all_query = "SELECT * FROM category WHERE category_id = $edit_cat_id";
    $select_all_result = mysqli_query($connection,$select_all_query);
    while($row = mysqli_fetch_assoc($select_all_result)){
        $cat_title = $row['category_title'];
        $cat_id = $row['category_id'];
?>
<div class="col-xl-6 col-lg-6" style="height: 200px;">
    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-item-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Edit Categories</h6>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <form action="category.php?id= <?php echo $cat_id; ?>" method="post">
                <!-- <label for="cat-title">Add Categories: </label> -->
                <input type="text" value="<?php echo $cat_title;?>" class="form-control" name="cat_title"><br>
                <input type="submit" class="btn btn-primary" value="update" name="update">
            </form>
        </div>
    </div>
</div>

<?php } } ?>

<?php
if(isset($_POST['update'])){
    $update_cat_title = $_POST['cat_title'];
    $update_cat_id = $_GET['id'];
    $update_query = "UPDATE category SET category_title = '$update_cat_title' WHERE category_id = $update_cat_id";
    $update_result = mysqli_query($connection, $update_query);
    if(!$update_result){
        die('QUERY FAILED').mysqli_error($connection);

    } else {
        header('location: category.php');
    }
}

?>
<?php

if(isset($_GET['delete'])){
    $delete_cat_id  = $_GET['delete'];
    $delete_query = "DELETE FROM category WHERE category_id = $delete_cat_id";
    $delete_result = mysqli_query($connection,$delete_query);
    header('location: category.php');
}

?>

<?php
include 'includes/footer.php'
?>

