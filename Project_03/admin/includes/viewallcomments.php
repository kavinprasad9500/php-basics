<div class="car shadow mb-4">
            <div class="car-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">View All Comments</h6>
            </div>
        
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Author</th>
                            <th>Email</th>
                            <th>Comments</th>
                            <th>Post</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th>Approved</th>
                            <th>UnApproved</th>
                            <!-- <th>Edit</th> -->
                            <th>Delete</th>

                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $select_all_query = "SELECT * FROM comments";
                        $select_all_result = mysqli_query($connection,$select_all_query);
                        while($row = mysqli_fetch_assoc($select_all_result)){
                            $comment_id = $row['comment_id'];
                            $comment_post_id = $row['comment_post_id'];
                            $comment_author = $row['comment_author'];
                            $comment_email = $row['comment_email'];
                            $comment_content = $row['comment_content'];
                            $comment_status = $row['comment_status'];
                            $comment_date = $row['comment_date'];
                            echo " <tr>
                                    <td>{$comment_id}</td>
                                    <td>{$comment_author}</td>
                                    <td>{$comment_email}</td>";
                                    
                        
                                    
                            echo  "<td>{$comment_content}</td>";
                            $select_comment_query = "SELECT * FROM posts WHERE post_id = $comment_post_id";
                            $select_comment_result = mysqli_query($connection,$select_comment_query);
                            while($row = mysqli_fetch_assoc($select_comment_result)){
                                $post_title = $row['post_title'];
                                $post_id = $row['post_id'];
                                echo "<td><a href='../blog-details.php?post={$post_id}'>{$post_title}</a></td>";
                            }


                            echo   "<td>{$comment_status}</td>
                                    <td>{$comment_date}</td>
                                    <td><a href='comments.php?approve={$comment_id}'>Approve</a></td>                                                    
                                    <td><a href='comments.php?unapprove={$comment_id}'>UnApprove</a></td>                                                   
                                    <td><a href='comments.php?delete={$comment_id}'>Delete</a></td>                                                    
                                    </tr>";
                             }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php


        if(isset($_GET['approve'])){
            $approve_comment_id =$_GET['approve'];
            $approve_query ="UPDATE comments SET comment_status = 'approved' WHERE comment_id = $approve_comment_id";
            $approve_result = mysqli_query($connection,$approve_query);
            header("location: comments.php");
            }
        if(isset($_GET['unapprove'])){
            $unapprove_comment_id =$_GET['unapprove'];
            $unapprove_query ="UPDATE comments SET comment_status = 'unapproved' WHERE comment_id = $unapprove_comment_id";
            $unapprove_result = mysqli_query($connection,$unapprove_query);        
            header("location: comments.php");
            }


        if(isset($_GET['delete'])){
            $delete_comment_id =$_GET['delete'];
            $delete_query ="DELETE FROM comments WHERE comment_id = $delete_comment_id";
            $delete_result = mysqli_query($connection,$delete_query);
            header("location: comments.php");
            }
        ?>