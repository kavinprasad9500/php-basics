<?php

class User {

    public $id;
    public $username;
    public $password;
    public $firstname;
    public $lastname;

    public static function show_all_users(){
        return self::query("SELECT * FROM users");

        // global $database;
        // $result = $database->query("SELECT * FROM users");
        // return $result;
    }
    public static function show_users_by_id($user_id){
        
        $array = self::query("SELECT * FROM users WHERE id = $user_id");
        return array_shift($array);
        // global $database;
        // $result = $database->query("SELECT * FROM users WHERE id = $user_id");
        // return $result;
    }

    public static function query($sql){
        global $database;
        $result = $database->query($sql);
        $array = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $array[] = self::auto_loop($row);
        }
        return $result;
    }
    public static function auto_loop($row){
        $user = new self;
        // $user->id            = $row['id'];
        // $user->username      = $row['username'];
        // $user->password      = $row['password'];
        // $user->firstname     = $row['firstname'];
        // $user->lastname      = $row['lastname'];
        foreach ($row as $name => $value){
            $user->$name = $value;
        }
        return $user;
    }
    public static function verify_user($username,$password){
        $array = self::query(" SELECT * FROM users WHERE username = '$username' AND password = '$password' ");
        return array_shift($array);
    }
    public function create($table,$fields,$values){
        global $database;
        $sql = "INSERT INTO $table($fields) VALUES ($values)";
        if($database->query($sql)){
            $this->id = mysqli_insert_id($database->conn);
            return true;
        }else{
            return false;
        }
    }
    public function update($table,$values){
        global $database;
        $sql = "UPDATE $table SET $values WHERE id = $this->id ";
        $database->query($sql);
        return mysqli_affected_rows($database->conn) == 0 ? true : false;

    }
    public function delete($table){
        global $database;
        $sql = "DELETE FROM $table WHERE id = $this->id";
        $database->query($sql);
    }
}


?>