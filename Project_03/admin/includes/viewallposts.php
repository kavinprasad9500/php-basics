<?php

    if(isset($_POST['selectBox'])){
        foreach($_POST['selectBox'] as $postIdValue){
            $select_option = $_POST['select_option'];
            
            switch($select_option){
                case 'published';
                    $published_query = "UPDATE posts SET post_status = '$select_option' WHERE post_id =$postIdValue";
                    $published_result = mysqli_query($connection,$published_query);
                    break;

                case 'unpublished';
                    $unpublished_query = "UPDATE posts SET post_status = '$select_option' WHERE post_id =$postIdValue";
                    $unpublished_result = mysqli_query($connection,$unpublished_query);
                    break;
                
                case 'delete';
                    $bulk_delete_query = "DELETE FROM posts WHERE post_id =$postIdValue";
                    $bulk_delete_result = mysqli_query($connection,$bulk_delete_query);
                    break;
            }
        }
    }

?>


<div class="car shadow mb-4 container">
        <div class="car-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> View All Posts</h6>
        </div>
        
        <div class="card-body">
            <form action="post.php" method="post" >
                <div class="row">
                <div class="col-xl-4" id="bulkoptions">
                <select name="select_option" class="form-control">
                    <option value="published">Publish</option>
                    <option value="unpublished">UnPublish</option>
                    <option value="delete">Delete</option>
                </select>
                <br>
                </div>
                <div class="col-xl-4">
                    <input type="submit" value="Apply" class="btn btn-sm btn-primary shadow-sm">
                </div>
                </div>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th><input type="checkbox" name="" id="selectAllBox"></th>
                            <th>Id</th>
                            <th>Author</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Tags</th>
                            <th>Comments</th>
                            <th>Date</th>
                            <th>Edit</th>
                            <th>Delete</th>

                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $select_all_query = "SELECT * FROM posts";
                        $select_all_result = mysqli_query($connection,$select_all_query);
                        while($row = mysqli_fetch_assoc($select_all_result)){
                            $post_id = $row['post_id'];
                            $post_title = $row['post_title'];
                            $post_author = $row['post_author'];
                            $post_date = $row['post_date'];
                            $post_content = $row['post_content'];
                            $post_image = $row['post_image'];
                            $post_tags = $row['post_tags'];
                            $post_comment_count = $row['post_comment_count'];
                            $post_status = $row['post_status'];
                            $post_category_id = $row['post_category_id'];
                            echo " <tr>";
                            ?>
                                    <td><input type="checkbox" class="checkboxs" name="selectBox[]"  value="<?php echo $post_id; ?>"></td>
                            <?php
                                echo "<td>{$post_id}</td>
                                    <td>{$post_author}</td>
                                    <td>{$post_title}</td>";
                                    
                            $select_all_cat_query = "SELECT * FROM category WHERE category_id=$post_category_id";
                            $select_all_cat_result = mysqli_query($connection,$select_all_cat_query);
                            while($row = mysqli_fetch_assoc($select_all_cat_result)){
                                $cat_title = $row['category_title'];
                                $cat_id = $row['category_id'];
                                echo "<td>{$cat_title}</td>";
                            }
                                    
                            echo "<td>{$post_status}</td>
                                    <td><img width='80px' height='50px' src='../images/$post_image' alt='images'/></td>
                                    <td>{$post_tags}</td>";

                                    $comment_count_query = "SELECT * FROM comments WHERE comment_post_id = $post_id AND comment_status = 'approved'";
                                    $comment_count_result = mysqli_query($connection,$comment_count_query);
                                    $comment_count = mysqli_num_rows($comment_count_result);

                            echo "<td>{$comment_count}</td>";

                            echo "  <td>{$post_date}</td>
                                    <td><a href='post.php?page=edit_post&post_id={$post_id}'>Edit</a></td>                                                    
                                    <td><a href='post.php?delete={$post_id}'>Delete</a></td>                                                    
                                    </tr>";
                             }
                        ?>
                    </tbody>
                </table>
                </form>
            </div>
        </div>
        <?php

        if(isset($_GET['delete'])){
            $delete_post_id =$_GET['delete'];
            $delete_query ="DELETE FROM posts WHERE post_id=$delete_post_id";
            $delete_result = mysqli_query($connection,$delete_query);
            header("location: post.php");
            }
        ?>