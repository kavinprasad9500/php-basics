<?php


class Database {
    public $conn;

    public function __construct(){
        $this->connect_db();
    }
    public function connect_db(){
        // $this->conn = mysqli_connect('localhost','root','','gallery');
        $this->conn = new mysqli('localhost','root','','gallery');
        if($this->conn->errno){
        // if(mysqli_connect_errno()){
            echo 'Not Connected To DataBase'. $this->conn->error;
        }
    
    }
    public function query($sql){
        // $result = mysqli_query($this->conn,$sql);
        // $result = $this->conn->query($this->conn,$sql);
        $result = $this->conn->query($sql);
        $this->confirm_query($result);
        return $result;
        
    }
    public function confirm_query($result){
        if(!$result){
            echo 'Query Failed'. $this->conn->error;
        }
        
    }
}
$database = new Database;
// $database->connect_db();


?>